package de.bilo

import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import groovy.json.StringEscapeUtils
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element

/**
 * Created by bul on 11.03.15.
 */
public class HbciToJSONConverter {
    static def docRoot = "/Volumes/SharedFolder/HBCI_HTML/HBCIID/"
    static def titleFile = "titles.json"
    static final String GROUP_NAME = "Group"
    static def defaultColumns = [
            "nr"    : "nr",
            "name"  : "name",
            "typ"   : "typ",
            "format": "format",
            "laenge": "laenge",
            "status": "status",
            "anzahl": "anzahl"
    ]
    static def additionalColumns = [
            "id"         : "id",
            "doc"        : "doc",
            "label"      : "label",
            "ref"        : "ref",
            "description": "description",
            "title"      : "title",
            "isMulti"    : "isMulti"

    ]
    static def columns = defaultColumns + additionalColumns;
    def static segmentGroupFile = ~/((?i)[A-Z]+S*\d+\.html)/
    def static titles = [:];
    static {
        File tit = new File(docRoot + titleFile);
        if (tit.exists()) {
            def jsonText = tit.text;
            titles = new JsonSlurper().parseText(jsonText);
        } else {
            new File(docRoot).eachFileRecurse { File f ->
                if (f.name.matches(segmentGroupFile)) {
                    def names = f.name.findAll("(?i)[A-Z]+S*");
                    titles[names[0].toUpperCase()] = parseTitle(f);
                }
            }
            tit.text = StringEscapeUtils.unescapeJavaScript(JsonOutput.prettyPrint(JsonOutput.toJson(titles)))
            println "${titles.size()}->${titles}"
        }

    }

    static def parseTitle(File file) {
        def title = "";
        Document doc = Jsoup.parse(file.getText("Cp1252"));
        title = doc.select(columns.title).first().text()
        title = title.replaceFirst(" Version .*", "")
        return title;
    }

    static def hbciToJson(String hbciDocPath, String hbciProtocol) {
        def segmentRegex = ~/((?:H|B|D|I)[A-Z]{4}S*:\d+:\d+)/
        def jsonArray = [:];
        def segs = hbciProtocol.split("\'");
        segs.each { segText ->
            segText = segText.replaceAll("\\s+", " ").trim()
            def segmentKoepfe = segText.findAll(segmentRegex)
            def segmentBody = [];

            segmentKoepfe.reverseEach { last ->
                segmentBody << segText.subSequence(segText.indexOf(last), segText.length())
                //first
            }

            segmentBody.each { String segment ->
                def props = segment.split("\\+");
                def metaInf = props[0].split(":");
                if (metaInf.length > 2) {
                    def gvName = metaInf[0];
                    //Segmentkennung, Segmentnummer, Segmentversion
                    def fileName = gvName.toLowerCase() + metaInf[2] + ".html";
                    List hbciProps = [];
                    Map res = [:]

                    def title = parseGvHtml(hbciDocPath, gvName, fileName, hbciProps)
                    res << ["title": title];
                    createSegmentJson(hbciProps, res, segment)
                    jsonArray[gvName] = res;

                } else {
                    if (debug)
                        println(" Skip Segment ${segment}")
                }
            }


        }
        def json = JsonOutput.toJson(jsonArray)

        StringEscapeUtils.unescapeJavaScript(JsonOutput.prettyPrint(json))
    }

    static def debug = true;

    public static void main(String[] args) {
        if (!debug) {
            if (args.size() < 2) {
                println("Usage = java -jar this.jar hbci doc folder HBCI-STRING")
            }
            File file;
            if (args.size() == 3) {

                file = new File(args[2])
            }
            String json = HbciToJSONConverter.hbciToJson(args[0], args[1])
            if (file != null) {
                file.text = json;
            }
        } else {

            File f = new File("/Volumes/SharedFolder/HBCI_HTML/gen-project/src/main/resources/single.txt");
            f = new File("/Volumes/SharedFolder/HBCI_HTML/gen-project/src/main/resources/test-proto1.txt");

            def String hbci = "HITAZS:96:1:4+800+1+1'\n" +
                    "HIPINS:97:1:4+1+1+1+5:12:6:Einwahlkontonummer::HKFGB:N:HKKAZ:N:HKSAL:N:HKWSD:N:HKWPR:N:HKWPD:N:HKWPO:J:HKWOA:J:HKWPS:J:HKWSO:N:HKOAN:N:HKWSD:N:HKWPR:N:HKKDM:J:HKGAM:N:HKPAE:J:HKTLA:J:HKTLF:J:HKTSP:N:HKTAZ:N:HKUMB:J:HKLSW:J:DKNTR:N:DKKND:J:HKTAN:N:HKSPA:N:HKCCS:J:HKCCM:J:HKDSE:J:HKBSE:J:HKDSC:J:HKDME:J:HKBME:J:HKDMC:J:HKCSE:J:HKCSB:N:HKCSA:J:HKCSL:J:HKCDE:J:HKCDB:N:HKCDN:J:HKCDL:J:HKCDU:J:HKDSB:N:HKDSW:J:HKCAZ:N:HKEKA:N:HKQTG:N:HKFRD:N:HKLWB:N:HKTAB:N:HKTAU:N:HKTSY:N:HKMTR:J:HKMTF:N:HKMTA:J:HKTML:N'\n";
            def String hbci2 =
                    "HIPINS:97:1:4+1+1+1+5:12:6:Einwahlkontonummer::HKFGB:N:HKKAZ:N:HKSAL:N:HKTML:N'\n";

            String json = HbciToJSONConverter.hbciToJson(docRoot, f.text)
            println(json)
        }
        //def segment = "HNVSK:998:3+PIN:1+998+1+2::3602830518804000ZMVT90OZKVSXT0+1:20150303:111539+2:2:13:@8@:5:1+280:38070724:950592317230:V:0:0+0'"


    }

    static def hbciToJsonTest(String hbciDocPath, String hbciProtocol) {
        def segs = hbciProtocol.split("\'");

        def segmentRegex = ~/((?:H|B|D|I)[A-Z]{4}S*)(?::\d+)*/
        segs.each { segText ->
            segText = segText.replaceAll("\\s+", " ").trim()
            def segmentKoepfe = segText.findAll(segmentRegex)
            def segmentBody = [];

            segmentKoepfe.reverseEach { last ->
                segmentBody << segText.subSequence(segText.indexOf(last), segText.length())
                //first
            }
            println(segmentBody)
        }
    }

    static def createSegmentJson(List segmentPropertyList, Map jsonResult, String segmentString) {
        String[] segmentValues = segmentString.split("\\+")
        def filledValuesSize = segmentValues.size();

        for (int i = 0; i < segmentPropertyList.size(); i++) {
            Map segmentProperty = segmentPropertyList[i];
            boolean isEmpty = (i >= filledValuesSize);
            if (segmentProperty[columns.typ] == GROUP_NAME) {
                if (isEmpty) {
                    jsonResult[segmentProperty[columns.label]] = []
                    jsonResult[columns.doc] = segmentProperty[columns.doc].toString();
                    jsonResult[columns.description] = segmentProperty[columns.description].toString();
                } else {
                    def groupString = segmentValues[i];
                    def groupRef = segmentProperty[columns.ref];
                    List groupProperty = segmentProperty[groupRef];
                    def groupJson = [:];
                    def multiGroup = [];
                    String[] minMax = segmentProperty[columns.anzahl].replaceAll("\\.+", ",").split(",")

                    if (minMax.size() == 0) {
                        minMax = [0, 0]
                    } else if (minMax.size() == 1) {
                        minMax = [minMax[0], minMax[0]]
                    }
                    int min = (minMax[0] as Integer);
                    int max = (minMax[1] as Integer);
                    boolean isMulti = max > 1

                    if (isMulti && !(groupString.isEmpty() || "\'".equals(groupString))) {

                        for (int j = 0; j <= max; j++) {
                            def json = [:]
                            //Wir müssen i incrementieren, damit obere schleife auch in die richtige position kommt
                            i = i + j;
                            if (i >= filledValuesSize) {
                                break;
                            }
                            groupString = segmentValues[i];
                            createGroupJson(groupProperty, json, groupString, min, min, multiGroup, false)
                            multiGroup << json;

                        }

                        jsonResult[segmentProperty[columns.label]] = multiGroup;
                    } else {
                        createGroupJson(groupProperty, groupJson, groupString, min, max, multiGroup, false)
                        //Wenn sich wiederholende gruppen in der gruppe vorhanden sind
                        isMulti = groupJson[columns.isMulti] != null
                        // createGroupJson();
                        if (isMulti) {
                            jsonResult[segmentProperty[columns.label]] = multiGroup;

                        } else {
                            jsonResult[segmentProperty[columns.label]] = groupJson;

                        }
                    }
                }
            } else {
                def hbciPropValue = "";
                if (!isEmpty) {
                    hbciPropValue = segmentValues[i]

                }
                jsonResult[segmentProperty[columns.label]] = hbciPropValue;
                jsonResult[columns.doc] = segmentProperty[columns.doc].toString();
                if (jsonResult[columns.title] == null)
                    jsonResult[columns.description] = segmentProperty[columns.description].toString();
            }
        }


    }

    static
    def createGroupJson(List groupPropertyList, Map groupJson, String groupString, int min, int max, List multiGroup,
                        def isMultiIteration) {
        def isList = max > 1;
        if (isList) {
            if (!isMultiIteration) {
                groupJson[columns.isMulti] = true;
            }
            groupJson = [:];
        }
        groupPropertyList.each { Map prop ->
            if (prop[columns.typ] == GROUP_NAME) {
                def subGroupRef = prop[columns.ref];
                List subGroupProperty = prop[subGroupRef];
                def subGroupJson = [:]
                String[] minMax = prop[columns.anzahl].replaceAll("\\.+", ",").split(",")
                if (minMax.size() == 0) {
                    minMax = [0, 0]
                } else if (minMax.size() == 1) {
                    minMax = [minMax[0], minMax[0]]
                }
                createGroupJson(subGroupProperty, subGroupJson, groupString, minMax[0] as Integer, minMax[1] as Integer, multiGroup, false)
                //Falls irgendwo ganz unten multi sind dann reichen wir gaanz nach oben
                if (subGroupJson[columns.isMulti] != null) {
                    groupJson[columns.isMulti] = subGroupJson[columns.isMulti];
                } else {
                    groupJson[prop[columns.label]] = subGroupJson

                }
            } else {
                int idx = groupString.indexOf(":")
                idx = idx < 0 && !groupString.isEmpty() ? groupString.length() : idx
                if (idx < 0)
                    return;

                def hbciPropValue = groupString.substring(0, idx);

                groupString = idx == groupString.length() ? "" : groupString.substring(idx + 1);
                groupJson[prop[columns.label]] = hbciPropValue;
                def tit = titles[hbciPropValue];
                if (tit != null) {
                    groupJson[columns.title] = tit;
                }

            }
        }
        if (isList) {
            multiGroup << groupJson;
        }
        if (isList && min < max && !groupString.isEmpty()) {
            if (!groupString.startsWith("\'")) {
                createGroupJson(groupPropertyList, [:], groupString, ++min, max, multiGroup, true)
            }
        }
    }

    static def parseGvHtml(def docPath, def identifier, def segFile, def props) {
        File input = new File(docPath + segFile);
        def title = "";
        try {
            Document doc = Jsoup.parse(input.getText("Cp1252"));
            title = doc.select("title").first().text()
            List<Element> trs = doc.select("tbody").select("tr");
            for (Element tr : trs) {
                def prop = [:];
                for (int i = 0; i < defaultColumns.keySet().size(); i++) {
                    def colName = defaultColumns[defaultColumns.keySet().getAt(i)]
                    Element td = tr.children().get(i);
                    if (colName == columns.name) {
                        def defineName = td.childNode(2).toString().trim();
                        prop[colName] = defineName;
                        prop[columns.id] = identifier;
                        prop[columns.doc] = input.toURI();
                        prop[columns.label] = GeneratorUtils.fieldNameFromDefine(defineName.replaceAll(identifier.toUpperCase(), ""))
                        prop[columns.description] = td.childNode(0).toString().trim()
                    } else if (colName == columns.format) {
                        prop[colName] = td.text().trim();
                        if (td.children().size() > 0) {
                            def link = td.select("a").first().attr("href");
                            if (link != null && link.endsWith(".html")) {
                                def prop1 = [];
                                def id = link.replaceAll(".html", "");
                                prop[columns.ref] = id

                                prop[id] = prop1;
                                parseGvHtml(docPath, id.replaceAll(/\d+/, ""), link, prop1)
                            }

                        }
                    } else {
                        prop[colName] = td.text();
                    }
                }
                props << prop;
            }

        } catch (IOException e) {
            if (debug)
                println(" Dokumentation nicht gefunden " + docPath + segFile)
        }
        title;
    }

}
