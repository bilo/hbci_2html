package de.bilo;

import java.util.regex.Pattern;

public class GeneratorUtils {

    public static String AbstractName(String src) {
        //' Erzeugt aus einem (versionsbehafteten) Gruppen- oder SegmentNamen
        //' einen entsprechenden nicht-versionsbehafteten
        //' z.B. aus "HKSAL5" wird "HKSAL"
        int i = src.length() - 1;
        for (; i >= 0; i--) {
            if (!Character.isDigit(src.charAt(i)))
                break;
        }
        return src.substring(0, i + 1);
    }

    public static String conVerNameToConstant(String src) {
        StringBuilder sb = new StringBuilder(src);
        int diff = 0;
        for (int i = 0; i < src.length(); i++) {
            char c = src.charAt(i);
            if (Character.isUpperCase(c) && i > 0) {
                sb.replace(i + diff, i + diff, "_");
                diff++;

            }

        }
        return ConstName(sb.toString());

    }

    public static String ConstName(String src) {
        // Erzeugt aus der Bezeichnung einer Gruppe einen
        // Java-Konstantenbezeichner (z.B.UNGEBUCHTE_UMSAETZE)
        char uml = '\u0308';
        String retVal = src.toUpperCase();
        retVal = retVal.replace(',', '_');
        retVal = retVal.replace('.', '_');
        retVal = retVal.replace(' ', '_');
        retVal = retVal.replace('-', '_');
        retVal = retVal.replace('/', '_');
        retVal = retVal.replace('(', '_');
        retVal = retVal.replace(')', '_');
        retVal = retVal.replaceAll("__", "_");
        retVal = retVal.replaceAll("A" + Character.toString(uml), "AE");
        retVal = retVal.replaceAll("Ä", "AE");
        retVal = retVal.replaceAll("U" + Character.toString(uml), "UE");
        retVal = retVal.replaceAll("Ü", "UE");
        retVal = retVal.replaceAll("Ö", "OE");
        retVal = retVal.replaceAll("O" + Character.toString(uml), "OE");
        retVal = retVal.replaceAll("ß", "SS");
        int plus = -1;
        while ((plus = retVal.indexOf('+')) >= 0) {
            retVal = retVal.substring(0, plus) + "UND" + retVal.substring(plus + 1);
        }
        return retVal;
    }

    public static void main(String[] args) {
        System.out.println(conVerNameToConstant("HalloWieGehtEsDirS"));
    }

    public static String ApiName(String src) {
        // Erzeugt aus einem Java-Konstantenbezeichner einer Gruppe
        // einen entsprechenden Member-Bezeichner (z.B. aus UNGEBUCHTE_UMSAETZE wird UngebuchteUmsaetze)
        // (wird für get/set gebraucht)

        boolean useUppercase = true;
        StringBuffer b = new StringBuffer(src.length());
        for (int i = 0; i < src.length(); i++) {
            char c = src.charAt(i);
            if (useUppercase) {
                b.append(Character.toUpperCase(c));
                useUppercase = false;
            } else {
                if (c == '_') {
                    useUppercase = true;
                } else {
                    b.append(Character.toLowerCase(c));
                }
            }
        }
        return b.toString();
    }

    public static final String PREFIX_REGEX = "(HBB_|HBO_)(%s_)*";

    public static String fieldNameFromDefine(def defineName) {

        String regEx = String.format(PREFIX_REGEX, ConstName(defineName));
        Pattern pattern = Pattern
                .compile(regEx,
                Pattern.MULTILINE);

        String[] titleArray = pattern.split(defineName);
        int count = titleArray.length > 1 ? 1 : 0;
        return fieldName(titleArray[count]);

    }

    public static String fieldName(String src) {
        // Erzeugt aus einem Java-Konstantenbezeichner einer Gruppe
        // einen entsprechenden Member-Bezeichner (z.B. aus UNGEBUCHTE_UMSAETZE wird UngebuchteUmsaetze)
        // (wird für get/set gebraucht)

        boolean useUppercase = false;
        StringBuffer b = new StringBuffer(src.length());
        for (int i = 0; i < src.length(); i++) {
            char c = src.charAt(i);
            if (useUppercase) {
                b.append(Character.toUpperCase(c));
                useUppercase = false;
            } else {
                if (c == '_') {
                    useUppercase = true;
                } else {
                    b.append(Character.toLowerCase(c));
                }
            }
        }
        return b.toString();
    }

    public static String ClassName(String src, boolean isSegment) {
        if (isSegment)
            return src;
        else
            return ApiName(ConstName(src));
    }

}